################
# Public subnet
################
resource "aws_subnet" "Pomelo_public" {
  count = var.create_vpc && length(var.public_subnets) > 0 && (false == var.one_nat_gateway_per_az || length(var.public_subnets) >= length(var.azs)) ? length(var.public_subnets) : 0

  vpc_id                          = aws_vpc.Pomelo_VPC.id
  cidr_block                      = element(concat(var.public_subnets, [""]), count.index)
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  map_public_ip_on_launch         = var.map_public_ip_on_launch

  tags = {
    Name              = var.public_subnet_name
    creator           = var.subnet_creator
    region            = var.subnet_region
    availability-zone = var.subnet_avail
    environment       = var.subnet_env
    atomation         = var.subnet_atomation
    account           = var.account_id
  }
}

output "subnet_id" {
  value = aws_subnet.Pomelo_public.id
}
